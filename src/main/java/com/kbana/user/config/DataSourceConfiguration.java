package com.kbana.user.config;

import com.kbana.postgres.PostgresDatabaseConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfiguration extends PostgresDatabaseConfig {
}
