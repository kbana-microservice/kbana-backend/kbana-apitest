package com.kbana.user.controller;

import com.kbana.bean.BaseResponse;
import com.kbana.bean.dto.group.CreateUserValid;
import com.kbana.bean.dto.user.UserDTO;
import com.kbana.exception.KbanaException;
import com.kbana.user.service.UserService;
import com.kbana.user.utils.PaginationUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(
            @RequestBody @Validated(value = CreateUserValid.class) UserDTO user)
            throws KbanaException {
        UserDTO userDTO = userService.createUser(user);
        return ResponseEntity.ok(userDTO);
    }

    @RequestMapping(value = "/detail", method = RequestMethod.POST)
    public ResponseEntity<?> getUser(
            @RequestParam(value = "id", required = false) Long id,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "phone", required = false) String phone
    ) throws KbanaException {
        log.info("get user for id={}, username={}, phone={}", id, username, phone);
        UserDTO userDTO;
        if (id != null) {
            userDTO = userService.getUserById(id);
        } else if (username != null) {
            userDTO = userService.getUserByUsername(username);
        } else {
            userDTO = userService.getUserByPhone(phone);
        }
        return ResponseEntity.ok(BaseResponse.of(userDTO));
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResponseEntity<?> update(@RequestBody UserDTO userDTO) throws KbanaException {
        userDTO = userService.updateUser(userDTO);
        return ResponseEntity.ok(BaseResponse.of(userDTO));
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ResponseEntity<?> search(@RequestBody UserDTO user, Pageable pageable) {
        Page<UserDTO> page = userService.search(user, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(
                ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(BaseResponse.of(page.getContent()));
    }

}
