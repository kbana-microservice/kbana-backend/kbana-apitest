package com.kbana.user.service;

import com.kbana.bean.dto.user.UserDTO;
import com.kbana.exception.KbanaException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {
    UserDTO createUser(UserDTO user) throws KbanaException;

    UserDTO getUserById(Long id) throws KbanaException;

    UserDTO getUserByUsername(String username) throws KbanaException;

    UserDTO getUserByPhone(String phone) throws KbanaException;

    UserDTO updateUser(UserDTO user) throws KbanaException;

    Page<UserDTO> search(UserDTO user, Pageable pageable);

}
