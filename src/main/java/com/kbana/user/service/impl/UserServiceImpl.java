package com.kbana.user.service.impl;

import com.kbana.bean.dto.user.UserDTO;
import com.kbana.constant.ResponseCode;
import com.kbana.exception.KbanaException;
import com.kbana.postgres.entity.KUser;
import com.kbana.postgres.entity.KUser_;
import com.kbana.postgres.repository.UserRepository;
import com.kbana.user.service.UserService;
import com.kbana.user.service.spectification.SearchCriteria;
import com.kbana.user.service.spectification.SearchOperation;
import com.kbana.user.service.spectification.SpecificationCustom;
import com.kbana.user.util.RollBackForException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.util.Strings;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private ModelMapper modelMapper;

//    private CacheManager cacheManager;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDTO createUser(UserDTO user) throws KbanaException {
        log.info("Create user: {}", user);
        KUser existUser = userRepository.findByUsername(user.getUsername());
        if (null != existUser) {
            log.info("Username: {} is already exist", user.getUsername());
            throw new KbanaException(ResponseCode.USE_NAME_ALREADY_USED);
        }
        String password = DigestUtils.md5Hex(user.getPassword()).toUpperCase();
        KUser createUser = KUser.builder()
                .username(user.getUsername())
                .fullname(user.getFullname())
                .email(user.getEmail())
                .phone(user.getPhone())
                .password(password)
                .build();

        KUser newUser = userRepository.save(createUser);
        log.info("Create user success");
        newUser.setPassword(Strings.EMPTY);
        return modelMapper.map(newUser, UserDTO.class);
    }

    @Override
    public UserDTO getUserById(Long id) throws KbanaException {
        log.info("get user by id = {}", id);
        Optional<KUser> option = userRepository.findById(id);
        if (option.isPresent()) {
            return modelMapper.map(option.get(), UserDTO.class);
        } else {
            log.info("user by id = {} is not found", id);
            throw new KbanaException(ResponseCode.USER_NOT_FOUND);
        }

    }

    @Override
    public UserDTO getUserByUsername(String username) throws KbanaException {
        log.info("get user by username = {}", username);
        KUser user = userRepository.findByUsername(username);
        if (user == null) {
            log.info("user by username = {} is not found", username);
            throw new KbanaException(ResponseCode.USER_NOT_FOUND);
        }
        user.setPassword("");
        return modelMapper.map(user, UserDTO.class);

    }

    @Override
    public UserDTO getUserByPhone(String phone) throws KbanaException {
        log.info("get user by phone = {}", phone);
        KUser user = userRepository.findByPhone(phone);
        if (user == null) {
            log.info("user by phone {} is not found", phone);
            throw new KbanaException(ResponseCode.USER_NOT_FOUND);
        }
        user.setPassword("");
        return modelMapper.map(user, UserDTO.class);

    }

    @Override
    @RollBackForException
//    @CacheEvict(value = "auth-user", key = "#userDTO.username")
    public UserDTO updateUser(UserDTO userDTO) throws KbanaException {
        log.info("begin update user {}", userDTO);
        Optional<KUser> optional = userRepository.findById(userDTO.getId());
        if (!optional.isPresent()) {
            log.info("can not find user by id ={}", userDTO.getId());
            throw new KbanaException(ResponseCode.USER_NOT_FOUND);
        }
        KUser exist = optional.get();
        exist.setEmail(userDTO.getEmail());
        exist.setFullname(userDTO.getFullname());
        exist.setPhone(userDTO.getPhone());
        exist = userRepository.save(exist);
        log.info("update user is success");
        exist.setPassword("");
        return modelMapper.map(exist, UserDTO.class);

    }

//    @Override
//    public Page<UserDTO> search(UserDTO user, Pageable pageable) {
//        log.info("search user with info {}, {}", user, pageable);
//        List<SearchCriteria> criteriaList = new ArrayList<>();
//        if (user.getUsername() != null) {
//            criteriaList.add(new SearchCriteria("username",
//                    user.getUsername(), SearchOperation.MATCH));
//        }
//        if (user.getEmail() != null) {
//            criteriaList.add(new SearchCriteria("email", user.getEmail(), SearchOperation.EQUAL));
//        }
//        if (user.getPhone() != null) {
//            criteriaList.add(new SearchCriteria("phone", user.getPhone(), SearchOperation.EQUAL));
//        }
//        Specification<KUser> specification = SpecificationCustom.build(criteriaList);
//        Page<UserDTO> page = userRepository.findAll(specification, pageable)
//                .map(u -> {
//                    u.setPassword("");
//                    return modelMapper.map(u, UserDTO.class);
//                });
//        log.info("search user result size {}", page.getContent().size());
//        log.debug("search user result {}", page);
//        return page;
//
//    }

    @Override
    public Page<UserDTO> search(UserDTO user, Pageable pageable) {
        log.info("search user with info {}, {}", user, pageable);

        Specification<KUser> userSpecification = SpecificationCustom.hasLike(KUser_.USERNAME, user.getUsername());
        Specification<KUser> phoneSpecification = SpecificationCustom.hasValue(KUser_.PHONE, user.getPhone());
        Specification<KUser> emailSpecification = SpecificationCustom.hasValue(KUser_.EMAIL, user.getEmail());
        Specification<KUser> condition = SpecificationCustom.and(userSpecification, phoneSpecification,
                emailSpecification);
        Page<UserDTO> page = userRepository.findAll(condition, pageable)
                .map(u -> {
                    u.setPassword("");
                    return modelMapper.map(u, UserDTO.class);
                });
        log.info("search user result size {}", page.getContent().size());
        log.debug("search user result {}", page);
        return page;

    }
}
