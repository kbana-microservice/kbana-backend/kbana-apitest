package com.kbana.user.service.spectification;

import org.apache.logging.log4j.util.Strings;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


public class SpecificationCustom {

    /**
     * Combine list of specification by and condition.
     * @param specifications list of specification need to combine
     * @param <T> Type
     * @return a combined {@link Specification}
     */
    @SafeVarargs
    public static <T> Specification<T> and(Specification<T> ...specifications) {
        if (null == specifications) return (root, query, cb) -> cb.conjunction();
        return (root, query, cb) -> {
            Predicate[] predicates = Arrays.stream(specifications)
                    .map(specification -> specification.toPredicate(root, query, cb))
                    .toArray(Predicate[]::new);
            return cb.and(predicates);
        };
    }

    /**
     * Combine list of specification by or condition.
     * @param specifications list of specification need to combine
     * @param <T> Type
     * @return a combined {@link Specification}
     */
    @SafeVarargs
    public static <T> Specification<T> or(Specification<T> ...specifications) {
        if (null == specifications) return (root, query, cb) -> cb.conjunction();
        return (root, query, cb) -> {
            Predicate[] predicates = Arrays.stream(specifications)
                    .map(specification -> specification.toPredicate(root, query, cb))
                    .toArray(Predicate[]::new);
            return cb.or(predicates);
        };
    }

    public static <T> Specification<T> hasValue(String key, String value) {
        return (root, query, cb) -> Strings.isBlank(value) ? cb.conjunction() : cb.equal(root.get(key), value);
    }

    public static <T> Specification<T> hasValue(String key, Integer value) {
        return (root, query, cb) -> value == null || value == 0 ? cb.conjunction() : cb.equal(root.get(key), value);
    }

    public static <T> Specification<T> hasValue(String key, Long value) {
        return (root, query, cb) -> value == null || value == 0 ? cb.conjunction() : cb.equal(root.get(key), value);
    }

    public static <T> Specification<T> hasLike(String key, String value) {
        return (root, query, cb) -> Strings.isBlank(value) ? cb.conjunction() : cb.like(root.get(key),
                "%" + value + "%");
    }

    public static <T> Specification<T> hasValueIn(String key, Collection value) {
        return (root, query, cb) -> root.get(key).in(value);
    }

    public static <T> Specification<T> hasValueNotIn(String key, Collection value) {
        return (root, query, cb) -> cb.not(root.get(key).in(value));
    }


    public static <T, V> Specification<V> hasJoin(String key1Join, JoinType joinType, String key2Join, String value) {
        return (root, query, cb) -> {
            if (Strings.isBlank(value)) {
                return cb.conjunction();
            }
            final Join<T, V> join = root.join(key1Join, joinType);
            return cb.equal(join.get(key2Join), value);
        };
    }

    public static <T, V> Specification<V> hasJoin(String key1Join, JoinType joinType, String key2Join, Long value) {
        return (root, query, cb) -> {
            if (null == value || value == 0) {
                return cb.conjunction();
            }
            final Join<T, V> join = root.join(key1Join, joinType);
            return cb.equal(join.get(key2Join), value);
        };
    }

    public static <T> Specification<T> greaterThan(String key, Timestamp fromDate) {
        return (root, query, cb) -> (fromDate == null) ? cb.conjunction() : cb.greaterThan(root.get(key), fromDate);
    }

    public static <T> Specification<T> lessThan(String key, Timestamp toDate) {
        return (root, query, cb) -> (toDate == null) ? cb.conjunction() : cb.lessThan(root.get(key), toDate);
    }

    public static <T> Specification<T> greaterThanOrEqualTo(String key, Timestamp fromDate) {
        return (root, query, cb) -> fromDate == null ? cb.conjunction() : cb.greaterThanOrEqualTo(root.get(key),
                fromDate);
    }

    public static <T> Specification<T> lessThanOrEqualTo(String key, Timestamp toDate) {
        return (root, query, cb) -> toDate == null ? cb.conjunction() : cb.lessThanOrEqualTo(root.get(key), toDate);
    }

    public static <T> Specification<T> between(String key, Timestamp fromDate, Timestamp toDate) {
        return (root, query, cb) -> (fromDate == null || toDate == null) ? cb.conjunction() : cb.between(root.get(key),
                fromDate, toDate);
    }

    public static <T> Specification<T> isNull(String key) {
        return (root, query, cb) -> cb.isNull(root.get(key));
    }

    public static <T> Specification<T> isNotNull(String key) {
        return (root, query, cb) -> cb.isNotNull(root.get(key));
    }

    public static <T> Specification<T> build(List<SearchCriteria> list) {
        return (Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            for (SearchCriteria criteria : list) {
                if (criteria.getOperation().equals(SearchOperation.GREATER_THAN)) {
                    predicates.add(builder.greaterThan(
                            root.get(criteria.getKey()), (Comparable) criteria.getValue()));
                } else if (criteria.getOperation().equals(SearchOperation.LESS_THAN)) {
                    predicates.add(builder.lessThan(root.get(criteria.getKey()), (Comparable) criteria.getValue()));
                } else if (criteria.getOperation().equals(SearchOperation.GREATER_THAN_EQUAL)) {
                    predicates.add(builder.greaterThanOrEqualTo(
                            root.get(criteria.getKey()), (Comparable) criteria.getValue()));
                } else if (criteria.getOperation().equals(SearchOperation.LESS_THAN_EQUAL)) {
                    predicates.add(builder.lessThanOrEqualTo(
                            root.get(criteria.getKey()), (Comparable) (criteria.getValue())));
                } else if (criteria.getOperation().equals(SearchOperation.NOT_EQUAL)) {
                    predicates.add(builder.notEqual(root.get(criteria.getKey()), criteria.getValue()));
                } else if (criteria.getOperation().equals(SearchOperation.EQUAL)) {
                    predicates.add(builder.equal(root.get(criteria.getKey()), criteria.getValue()));
                } else if (criteria.getOperation().equals(SearchOperation.MATCH)) {
                    predicates.add(builder.like(builder.lower(root.get(criteria.getKey())),
                            "%" + criteria.getValue().toString().toLowerCase() + "%"));
                } else if (criteria.getOperation().equals(SearchOperation.MATCH_END)) {
                    predicates.add(builder.like(builder.lower(root.get(criteria.getKey())),
                            criteria.getValue().toString().toLowerCase() + "%"));
                } else if (criteria.getOperation().equals(SearchOperation.MATCH_START)) {
                    predicates.add(builder.like(builder.lower(root.get(criteria.getKey())),
                            "%" + criteria.getValue().toString().toLowerCase()));
                } else if (criteria.getOperation().equals(SearchOperation.IN)) {
                    predicates.add(builder.in(root.get(criteria.getKey())).value(criteria.getValue()));
                } else if (criteria.getOperation().equals(SearchOperation.NOT_IN)) {
                    predicates.add(builder.not(root.get(criteria.getKey())).in(criteria.getValue()));
                }
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        };
    }

}
